﻿using Cloudmint.Cpverify.Areas.Account.Models;
using Cloudmint.Cpverify.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cloudmint.Cpverify.Areas.Account.Controllers
{
    public class LoginController : Controller
    {
        // GET: Account/Login
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

       // UserLoginModel input
        public JsonResult AuthenticateLogin(string username, string password)
        {
            string resp = string.Empty;
            try
            {
                Logger.Info("Validating credentials");
                string serviceurl = string.Concat(ConfigurationManager.AppSettings["service_url"].ToString(), "CPVGridService/authenticateUser");
                StringBuilder enSb = new StringBuilder();
                StringWriter ensw = new StringWriter(enSb);
                using (JsonWriter writer = new JsonTextWriter(ensw))
                {
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartObject();
                    writer.WritePropertyName("USERNAME");
                    writer.WriteValue(username);
                    writer.WritePropertyName("PASSWORD");
                    writer.WriteValue(password);

                    writer.WriteEndObject();
                }
                Logger.Info("sending request to server for authenticating");
                string response = JService.GetServiceResponse(serviceurl, enSb.ToString());

                Dictionary<string, string> tblResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);
                if (string.IsNullOrEmpty(tblResponse["ERROR"]))
                {

                    LoginInfo _info = new LoginInfo
                    {
                        CLIENT_ID = Convert.ToString(tblResponse["CLIENT_ID"].ToString()),
                        CLIENT_NAME = Convert.ToString(tblResponse["CLIENT_NAME"]),
                        USER_NAME = Convert.ToString(tblResponse["USER_NAME"]),
                        FULL_NAME = Convert.ToString(tblResponse["FULL_NAME"]),
                        ROLE_NAME = Convert.ToString(tblResponse["ROLE_NAME"])
                    };
                    Session["info"] = _info;
                    FormsAuthentication.Initialize();
                    FormsAuthenticationTicket ftick = new FormsAuthenticationTicket(1, _info.USER_NAME, DateTime.Now, DateTime.Now.AddMinutes(60), true, Convert.ToString(_info.USER_NAME));
                    FormsAuthentication.SetAuthCookie(_info.USER_NAME, false);
                    resp = "SUCCESS";
                }
                else
                    resp = Convert.ToString(tblResponse["ERROR"]);

            }
            catch (Exception ex)
            {
                Logger.Error("Error in authenticating the user credentials", ex);
            }
            return Json(resp, JsonRequestBehavior.AllowGet);
        }


    }

    public class UserLoginModel
    {
        public string USERNAME { get; set; }

        public string PASSWORD { get; set; }
    }

}