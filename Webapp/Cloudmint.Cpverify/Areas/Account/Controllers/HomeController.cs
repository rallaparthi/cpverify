﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cloudmint.Cpverify.Areas.Account.Controllers
{
    public class HomeController : Controller
    {
        // GET: Account/Home
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}