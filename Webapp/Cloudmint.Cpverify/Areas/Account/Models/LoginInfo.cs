﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cloudmint.Cpverify.Areas.Account.Models
{
    [Serializable]
    public class LoginInfo
    {
        public string CLIENT_ID { get; set; }

        public string CLIENT_NAME { get; set; }

        public string USER_NAME { get; set; }

        public string FULL_NAME { get; set; }

        public string ROLE_NAME { get; set; }

      
    }
}