﻿using System.Web.Mvc;

namespace Cloudmint.Cpverify.Areas.Account
{
    public class AccountAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Account";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Account_default",
                "Account/{controller}/{action}/{id}",
                new { Controller="Login", action = "Index", id = UrlParameter.Optional },
                namespaces:new[] {"Cloudmint.Cpverify.Areas.Account.Controllers"}
            );
        }
    }
}