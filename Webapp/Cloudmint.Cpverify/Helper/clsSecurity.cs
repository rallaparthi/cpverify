﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Cloudmint.Cpverify.Helper
{
    public class Security
    {
        /// <summary>
        /// Text encryption
        /// </summary>
        /// <param name="sPassword"></param>
        /// <returns></returns>
        public static string Pass_Encrypt(string sPassword)
        {
            MD5CryptoServiceProvider xpass = new MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(sPassword);
            bs = xpass.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }

    }
}