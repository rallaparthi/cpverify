﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Cloudmint.Cpverify.Helper
{
    public static class JService
    {
        public static string Get_UniqueID()
        {
            int seed = (int)DateTime.Now.Ticks;
            Random number = new Random(seed);
            string id = string.Format("{0}{1}", DateTime.Today.ToString("yyyyMMdd"), number.Next().ToString("D10"));
            return id;
        }


        //public static string GetServiceResponse(string url)
        //{
        //    try
        //    {
        //        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(url);

        //        ASCIIEncoding encoding = new ASCIIEncoding();
        //        Models.UserInfoModel userinfo = (Models.UserInfoModel)HttpContext.Current.Session["info"];

        //        byte[] data = encoding.GetBytes("{\"CLIENT_ID\":\""+ userinfo.Account_id + "\"}");
        //        httpWReq.Method = "POST";
        //        httpWReq.ContentType = "application/json";
        //        httpWReq.ContentLength = data.Length;
        //        Stream stream = httpWReq.GetRequestStream();
        //        stream.Write(data, 0, data.Length);
        //        stream.Close();
        //        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
        //        string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //        return responseString;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error in getting response from service", ex);
        //        return null;
        //    }

        //}


        public static string GetServiceResponse(string url, string postData)
        {
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(url);

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);

                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";
                httpWReq.ContentLength = data.Length;
                Stream stream = httpWReq.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (Exception ex)
            {
                Logger.Error("Error in getting response from service", ex);
                return null;
            }

        }



    }
}