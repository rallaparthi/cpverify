﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cloudmint.Cpverify.Helper
{
    public static class Logger
    {
        static log4net.ILog logger = log4net.LogManager.GetLogger("File");

        public static void Error(string message, Exception ex)
        {
            logger.Error(string.Concat(message, string.Format("Exception: {0}, InnerException {1}, StackTrace: {2}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : string.Empty, ex.StackTrace)));
        }

        public static void Info(string message)
        {
            logger.Info(message);
        }

        public static void Debug(string message)
        {
            logger.Debug(message);
        }


    }
}